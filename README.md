# WHG Games 

Basic app based on Lumen which gives you possibility to fetch games listing by providing country code and brand identifier
## Prerequirements

To build & run this project you have to make sure that you have `Docker` and `Docker Compose` installed.

### Create .env file
Before you start please create .env file based on .env.example and provide the necessary variables.

`cp .env.example .env`

## Build & run containers
Build image `docker-compose build` 

Run containers in detached mode `docker-compose up -d`

### Compile assets
```
npm run dev - for development purposes
npm run watch - to watch file changes
npm run production - for production purposes
```

### Run tests
`./vendor/bin/phpunit`
