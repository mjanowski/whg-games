<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <title>WHG</title>
    <link href="{{ url('/css/app.css') }}" rel="stylesheet">
</head>
<body>

<main>
    <div class="container">
        <div class="row">
            <h1 class="center">Search games</h1>
            <form id="getGamesForm" class="search-games-form" data-config="{{ $configApiUrl }}" data-submit="{{ $submitApiUrl }}">
                <div class="row">
                    <div class="col-6">
                        <select name="country" class="search-games-form__select" id="countryCode" required>
                            <option value="">Please select country</option>
                        </select>
                    </div>
                    <div class="col-6">
                        <select name="brand" class="search-games-form__select" id="brandId" required>
                            <option value="">Please select brand</option>
                        </select>
                    </div>
                    <div class="col-12">
                        <button type="submit" class="search-games-form__submit">
                            Go!
                        </button>
                    </div>
                </div>
            </form>
            <div id="gamesListing" class="games-listing">

            </div>
        </div>
    </div>
</main>

<script src="{{ url('/js/app.js') }}"></script>
</body>
</html>
