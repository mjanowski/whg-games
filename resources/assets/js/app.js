(function () {

    var init = function(){
            fetchFormConfig();
            fetchGamesListing();
        },

        fetchFormConfig = function() {
            var form = document.getElementById('getGamesForm'),
                countrySelectbox = document.getElementById('countryCode'),
                brandSelectbox = document.getElementById('brandId');

            fetch(form.dataset.config)
                .then(response => response.json())
                .then(data => {
                    for( let prop in data.countries.data ) {
                        let item = data.countries.data[prop],
                            option = document.createElement('option');
                        option.value = data.countries.data[prop].code;
                        option.text = data.countries.data[prop].country;
                        countrySelectbox.appendChild(option)
                    }

                    for( let prop in data.brands.data ) {
                        let item = data.brands.data[prop],
                            option = document.createElement('option');
                        option.value = data.brands.data[prop].id;
                        option.text = data.brands.data[prop].brand;
                        brandSelectbox.appendChild(option)
                    }
                });
        },
        fetchGamesListing = function() {
            var form = document.getElementById('getGamesForm'),
                countrySelectbox = document.getElementById('countryCode'),
                brandSelectbox = document.getElementById('brandId'),
                listingContainer = document.getElementById('gamesListing');

            form.addEventListener('submit', fetchData)

            function fetchData(event) {
                listingContainer.innerHTML = ''
                fetch(form.dataset.submit + '/' + brandSelectbox.value + '/' + countrySelectbox.value)
                    .then(response => response.json())
                    .then(data => {
                        for( let prop in data.data ) {
                            let item = data.data[prop],
                                img = document.createElement('img'),
                                title = document.createElement('h3'),
                                wrapper = document.createElement('div')

                            img.setAttribute('class', 'games-listing__img')
                            title.setAttribute('class', 'games-listing__title')
                            wrapper.setAttribute('class', 'games-listing__item')

                            title.innerHTML = item.name;
                            img.setAttribute('src', item.image)
                            wrapper.appendChild(img)
                            wrapper.appendChild(title)
                            listingContainer.appendChild(wrapper)
                        }
                    });
                event.preventDefault();
            }
        };

    window.onload = init();
})();
