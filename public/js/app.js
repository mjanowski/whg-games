/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 0);
/******/ })
/************************************************************************/
/******/ ({

/***/ "./resources/assets/js/app.js":
/*!************************************!*\
  !*** ./resources/assets/js/app.js ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

(function () {
  var init = function init() {
    fetchFormConfig();
    fetchGamesListing();
  },
      fetchFormConfig = function fetchFormConfig() {
    var form = document.getElementById('getGamesForm'),
        countrySelectbox = document.getElementById('countryCode'),
        brandSelectbox = document.getElementById('brandId');
    fetch(form.dataset.config).then(function (response) {
      return response.json();
    }).then(function (data) {
      for (var prop in data.countries.data) {
        var item = data.countries.data[prop],
            option = document.createElement('option');
        option.value = data.countries.data[prop].code;
        option.text = data.countries.data[prop].country;
        countrySelectbox.appendChild(option);
      }

      for (var _prop in data.brands.data) {
        var _item = data.brands.data[_prop],
            _option = document.createElement('option');

        _option.value = data.brands.data[_prop].id;
        _option.text = data.brands.data[_prop].brand;
        brandSelectbox.appendChild(_option);
      }
    });
  },
      fetchGamesListing = function fetchGamesListing() {
    var form = document.getElementById('getGamesForm'),
        countrySelectbox = document.getElementById('countryCode'),
        brandSelectbox = document.getElementById('brandId'),
        listingContainer = document.getElementById('gamesListing');
    form.addEventListener('submit', fetchData);

    function fetchData(event) {
      listingContainer.innerHTML = '';
      fetch(form.dataset.submit + '/' + brandSelectbox.value + '/' + countrySelectbox.value).then(function (response) {
        return response.json();
      }).then(function (data) {
        for (var prop in data.data) {
          var item = data.data[prop],
              img = document.createElement('img'),
              title = document.createElement('h3'),
              wrapper = document.createElement('div');
          img.setAttribute('class', 'games-listing__img');
          title.setAttribute('class', 'games-listing__title');
          wrapper.setAttribute('class', 'games-listing__item');
          title.innerHTML = item.name;
          img.setAttribute('src', item.image);
          wrapper.appendChild(img);
          wrapper.appendChild(title);
          listingContainer.appendChild(wrapper);
        }
      });
      event.preventDefault();
    }
  };

  window.onload = init();
})();

/***/ }),

/***/ "./resources/assets/sass/app.scss":
/*!****************************************!*\
  !*** ./resources/assets/sass/app.scss ***!
  \****************************************/
/*! no static exports found */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),

/***/ 0:
/*!***************************************************************************!*\
  !*** multi ./resources/assets/js/app.js ./resources/assets/sass/app.scss ***!
  \***************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(/*! /home/mjanowski/Desktop/projects/whg/resources/assets/js/app.js */"./resources/assets/js/app.js");
module.exports = __webpack_require__(/*! /home/mjanowski/Desktop/projects/whg/resources/assets/sass/app.scss */"./resources/assets/sass/app.scss");


/***/ })

/******/ });