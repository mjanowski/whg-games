<?php

use Laravel\Lumen\Testing\DatabaseMigrations;
use Laravel\Lumen\Testing\DatabaseTransactions;

class GamesControllerTest extends TestCase
{
    /**
     * Games API tests.
     *
     * @return void
     */

    /**
     * Test proper games listing
     * response
     */
    public function testGamesListingResponse()
    {
        $brand = 5;
        $country = 'DE';

        $this->json('GET', "/api/games/$brand/$country")
            ->seeJson([
                'status' => 'error',
            ], true);
    }

    /**
     * Test proper search config
     * response
     */
    public function testSearchConfigResponse()
    {
        $brand = 5;
        $country = 'DE';

        $this->json('GET', "/api/games/search-config-data")
            ->seeJson([
                'status' => 'error',
            ], true);
    }

}
