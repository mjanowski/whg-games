<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/','FrontController@indexAction');
$router->get('/api/games/{brand}/{country}','GamesController@gamesListingAction');
$router->get('/api/games/search-config-data','GamesController@searchConfigDataAction');

