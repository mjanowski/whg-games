<?php

namespace App\Services;

use Illuminate\Support\Facades\DB;

/**
 * Class GameService
 * Provides methods to fetch games data
 *
 * @package App\Services
 */
class GameService
{
    /**
     * Get games for given brand and country
     *
     * @param string $brandId - brand identifier
     * @param string $countryCode - country code for ex. DE, US, GB
     * @return \Illuminate\Support\Collection
     */
    public function getBrandCountryGames(string $brandId, string $countryCode)
    {
        $blockedCountryGames = DB::table('game_country_block')
                            ->where('country', '=', $countryCode);

        return DB::table('game')
                    ->select(
                        'game.*',
                        'game_providers.name as gameProviderName',
                        'game_providers.distributor as gameProviderDistributor',
                        'brands.brand as brandName'
                    )
                    ->join('brand_games', 'game.launchcode', '=', 'brand_games.launchcode')
                    ->leftJoinSub($blockedCountryGames, 'game_country_block', function ($join) {
                        $join->on('game.launchcode', '=', 'game_country_block.launchcode');
                    })
                    ->join('brands', 'brand_games.brandid', '=', 'brands.id')
                    ->join('game_providers', 'game.game_provider_id', '=', 'game_providers.id')
                    ->leftJoin('game_brand_block', 'game.launchcode', '=', 'game_brand_block.launchcode')
                    ->whereNull('game_brand_block.blocked_date')
                    ->whereNull('game_country_block.blocked_date')
                    ->where('brands.id', '=', $brandId)
                    ->get();
    }
}
