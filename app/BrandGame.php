<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BrandGame extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'brand_games';
}
