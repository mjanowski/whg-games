<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Game extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'image' => env('GAMES_IMAGES_PATH') . $this->launchcode . '.jpg',
            'gameProviderName' => $this->gameProviderName,
            'gameProviderDistributor' => $this->gameProviderDistributor,
            'brandName' => $this->brandName
        ];
    }
}
