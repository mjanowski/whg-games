<?php

namespace App\Http\Controllers;

class FrontController extends Controller
{
    /**
     * Displays frontend form view / GUI
     *
     * @return \Illuminate\View\View|\Laravel\Lumen\Application
     */
    public function indexAction()
    {
        return view(
            'Default',
            [
                'configApiUrl' => url('/api/games/search-config-data'),
                'submitApiUrl' => url('/api/games/')
            ]
        );
    }
}
