<?php

namespace App\Http\Controllers;

use App\Brand;
use App\Country;
use App\Http\Resources\BrandCollection;
use App\Http\Resources\CountryCollection;
use App\Http\Resources\GameCollection;
use App\Services\GameService;

class GamesController extends Controller
{
    /**
     * @var GameService
     */
    protected $gameService;


    /**
     * GamesController constructor.
     *
     * @param GameService $gameService
     */
    public function __construct(GameService $gameService)
    {
        $this->gameService = $gameService;
    }

    /**
     * Returns games listing based on
     * given brand id and country code
     *
     * @param string $brand
     * @param string $country
     * @return \Illuminate\Http\JsonResponse
     */
    public function gamesListingAction(string $brand, string $country)
    {
        try {
            $gamesData = $this->gameService->getBrandCountryGames($brand, $country);
            $response = new GameCollection($gamesData);
        } catch (\Exception $exception) {
            $response = [
                'status' => 'error',
                'message' => $exception->getMessage()
            ];
        }

        return response()->json($response);
    }

    /**
     * Returns config (countries and brands)
     * for search form selectboxes
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function searchConfigDataAction()
    {
        try {
            $response = [
                'countries' => new CountryCollection(Country::all()),
                'brands' => new BrandCollection(Brand::all()),
            ];
        } catch (\Exception $exception) {
            $response = [
                'status' => 'error',
                'message' => $exception->getMessage()
            ];
        }

        return response()->json($response);
    }
}
