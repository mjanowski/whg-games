<?php

namespace App\Providers;

use App\Services\GameService;
use Illuminate\Support\ServiceProvider;

class GameServiceProvider extends ServiceProvider
{
    /**
     * Register game service.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(GameService::class, function () {
            return new GameService();
        });
    }
}
