<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GameProvider extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'game_providers';
}
